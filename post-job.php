<?php
session_start();
    if(empty($_SESSION['comemail'])){
        header("Location:index.php");
    }

?>
 
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <?php include 'pages/head.php'; ?>
     <title>Upload New Job</title>
 </head>
 <body>
     <?php include 'pages/nav.php'; ?>
    <br>
    <div class="container">
        <div class=" col-md-6 offset-md-3">
            
            <br>
            <h3 class="text-center">Upload New job</h3>
            <br>
            <form id="uploadjob">
                <div class="sms"></div>
                <div class="mb-3">
                    <input type="text" placeholder="Job Title" id="title" class="form-control">
                </div>
                <div class="mb-3">
                    <input type="text" placeholder="Salary" id="salary" class="form-control">
                </div>
                <div class="mb-3">
                    <textarea name="" id="description" cols="30" rows="10" class="form-control" placeholder="job description"></textarea>
                </div>
                <div class="mb-3">
                    <input type="text" placeholder="Address" id="address" class="form-control">
                </div>
                <div class="mb-3">
                    <input type="text" placeholder="Phone" id="phone" class="form-control">
                </div>
                <input type="hidden" id="email" value="<?php echo $_SESSION['comemail']; ?>">
                <button class="btn btn-primary ujbtn" type="submit">Upload</button>
            </form>
            <div class="sms"></div>
        </div>
    </div>


     <br>



     <?php include 'pages/footer.php'; ?>
     <script src="resource/js/job.js"></script>
     
 </body>
 </html>