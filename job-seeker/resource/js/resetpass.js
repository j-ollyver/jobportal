// reset password form submit
const resetpass = document.querySelector("#resetpass");
resetpass.addEventListener('submit', function (e){
    e.preventDefault();

    let email= document.querySelector('#email').value;
    if(!email){
        alert('email is reqired');
    }else{
        $.ajax({
            url:"exe/request-resent-link.php",
            type:"POST",
            data:{bemail:email},
            beforeSend:function(){
                $('#resbtn').html('Please wait');
            },
            success:function(res){
                $('.sms').html(res);
            }
        });
    }
});